package aura

import (
	"github.com/gammazero/nexus/router"
	"github.com/gammazero/nexus/wamp"
	"log"
	"os"
	"os/signal"
)

var auraLogger = log.New(os.Stdout, "Aura > ", log.LstdFlags)

type Server struct {
	Domain          string
	Port            int
	Realms          []string
	IsTLS           bool
	RouterEvent     RouterEvent
	WSServer        *router.WebsocketServer
	RawSocket       *router.RawSocketServer
	Logger          *log.Logger
	WL              []string
}

type Settings struct {
	concurrent bool
}

type Option struct {
	Concurrent bool
	Args       []interface{}
	Apply      func(s *Server, args []interface{}) error
}

type RouterEvent struct {
	c chan RouterAction
}

type RouterAction struct {
	EventType string
	Payload interface{}
}

func (s *Server) SetWSServer(url string) {
	s.RouterEvent.c <- RouterAction{"AddWS", url}
}

func (s *Server) RouterShutDown() {
	s.RouterEvent.c <- RouterAction{"ShutDown", nil}
}

func (s *Server) New(options ...Option) *Server {
	var err error = nil
	for _, opt := range options {
		if opt.Concurrent {
			go opt.Apply(s, opt.Args)
		} else {
			err = opt.Apply(s, opt.Args)
		}
		if err != nil {
			s.Logger.Panicln("Failed to load Option: ", err)
		}
	}
	auraLogger.Println("New Server Setting for", s.Domain, "on port", s.Port)
	return s

}

func SetRouter(s *Server, args []interface{}) error {

	s.RouterEvent.c = make(chan RouterAction, 0)

	var realmConfigs []*router.RealmConfig

	for _, realm := range s.Realms {

		config := router.RealmConfig{
			URI: wamp.URI(realm),
			StrictURI: true,
			AnonymousAuth: true,
		}

		realmConfigs = append(realmConfigs, &config)
	}

	routerConfig := &router.Config{
		RealmConfigs: realmConfigs,
	}

	nxr, err := router.NewRouter(routerConfig, s.Logger)
	if err != nil {
		s.Logger.Panicln("Failed to start new Router: ", err)
		return err
	}
	defer nxr.Close()
	defer s.Logger.Panicln("Closing Router")

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)

	for event := range s.RouterEvent.c {

		s.Logger.Println("Listening to WS", event)

		switch event.EventType {
			case"AddWS":

				wsUrl := event.Payload.(string)
				s.WSServer = router.NewWebsocketServer(nxr)

				err = s.WSServer.AllowOrigins(s.WL)
				if err != nil {
					auraLogger.Panicln(err)
				}

				_, err := s.WSServer.ListenAndServe(wsUrl)
				if err != nil {
					auraLogger.Panicln(err)
				}

				auraLogger.Println("Listening to ws://" +  wsUrl)

				continue

			case"ShutDown":

				s.Logger.Println("Shutting Down Router")

				break
			}
		}

	return nil
}
