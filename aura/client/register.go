package client

import (
	"github.com/gammazero/nexus/client"
	"github.com/gammazero/nexus/wamp"
)

func (a *AuraClient) RegisterService(procedureName string, handler client.InvocationHandler, options wamp.Dict) {

	err := a.Client.Register(procedureName, handler, options)

	if err != nil {

		a.Logger.Panicln("Failed to register procedure: ", procedureName, err)

	} else {

		a.Logger.Println("Procedure Registered: ", procedureName)
	}
}