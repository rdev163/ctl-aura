package client

type Options struct {
	Sender string
	Origin string
	Nonce string
}

type Message struct {
	Type string
	Payload interface{}
	Options Options
}
