package client

import (
	"fmt"
	"github.com/gammazero/nexus/client"
	"log"
	"os"
)

var clientLogger = log.New(os.Stdout, "Client > ", log.LstdFlags)

type AuraClient struct {
	Realm string
	URL string
	Client *client.Client
	Logger *log.Logger
	Reporter chan string
}

type option func(auraClient *AuraClient) error

func (a *AuraClient) New(options ...option) *AuraClient {
	for _, opt := range options {

		err := opt(a)
		if err != nil {
			a.Logger.Panicln(err)
		}
	}
	return a
}

func ConnectWS(auraClient *AuraClient) error {
	clientLogger.Println("Connecting Client with WS: ", auraClient)
	serialization := client.JSON
	netAddress := fmt.Sprintf("ws://%s", auraClient.URL)
	config := client.Config{
		Realm: auraClient.Realm,
		Serialization: serialization,
		Logger: clientLogger,
	}

	auraClient.Logger.Println(netAddress, config)

	cli, err := client.ConnectNet(netAddress, config)
	if err != nil {

		auraClient.Logger.Panicln("Failed to ConnectNet: ", err)
		return err
	}
	auraClient.Client = cli
	auraClient.Logger.Println("WS Successfully connected: ", auraClient.URL, auraClient.Realm)
	return nil
}

