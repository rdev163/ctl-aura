FROM ubuntu:18.04

ENV GOPATH /go
ENV PATH /go/bin:$PATH

CMD ["nginx", "-g", "daemon off;"]
CMD /go/src/a2j-aura-router/aura

RUN apt-get update; yes | apt-get upgrade; yes | apt-get install golang nginx git
RUN mkdir -p /go/src /go/bin && chmod -R 777 /go

COPY . /go/src/a2j-aura-router/
COPY docker/configs/nginx.ws /etc/nginx/sites-available/default

WORKDIR /go/src/a2j-aura-router/

RUN go get -d github.com/gammazero/nexus/...
RUN go build aura.go

EXPOSE 80