package main

import (
	"fmt"
	"github.com/CodeForPortland/ctl-aura/aura"
	"github.com/spf13/viper"
	"log"
	"os"
	"os/signal"
)

type localServerConfig struct {
	domain string
	port int
	realms []string
	allowedOrigins []string
}

var logger = log.New(os.Stdout, "Aura Main > ", log.LstdFlags)

func main() {
	config := getLocalServerConfig()

	s := aura.Server{
		Domain: config.domain,
		Port: config.port,
		Realms: config.realms,
		Logger: logger,
		WL: config.allowedOrigins,
	}

	s.New(aura.Option{Concurrent: true, Args: nil, Apply: aura.SetRouter})

	wsURL := fmt.Sprintf("%s:%d", config.domain, config.port)

	go s.SetWSServer(wsURL)

	sigChan := make(chan os.Signal, 1)

	signal.Notify(sigChan, os.Interrupt)

	select {

		case <-sigChan:

			logger.Println("Shutting Down!")

			s.RouterShutDown()
	}
}

var defaultConfigs = localServerConfig{
	domain: "localhost",
	port: 3131,
	realms: []string{"aura.ctl.public"},
	allowedOrigins: []string{"*"},
}

// todo set default values
func getLocalServerConfig() localServerConfig {

	viper.SetConfigName("server.config")

	viper.AddConfigPath("./local")

	err := viper.ReadInConfig()

	if err != nil {
		logger.Println("Failed to read server.config.json in ./local, make sure one is there. Using default config.")
		return defaultConfigs
	}

	response := localServerConfig{
		domain: viper.Get("domain").(string),
		port: int(viper.Get("port").(float64)),
		realms: viper.GetStringSlice("realms"),
		allowedOrigins: viper.GetStringSlice("allowedOrigins"),
	}

	return response
}