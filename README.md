# AURA - an Agnostic Utility for Remote Applications
> A WIP Expect changes without warning and unstable components. Any questions ask [Rex](iamrexdev@gmail.com)

[![Waffle.io - Columns and their card count](https://badge.waffle.io/CodeForPortland/CTL-AURA.svg?columns=all)](https://waffle.io/CodeForPortland/CTL-AURA)

Working with a variety of developers we think that contributions should be agnostic to languages the developers want to use. 


# AURA-CTL
> Built from [Nexus](https://github.com/gammazero/nexus/wiki)

## GoTooling: 

Clients can now be setup from the `aura/client` package for using in services using the `import()` functionality.

## Server setup and Options

### Config

`Main.go` consumes a `local/local.config.json` which shape looks like... 

```json
{
  "domain": "localhost",
  "port": 3131,
  "realms": ["aura.ctl.public"],
  "allowedOrigins": ["*"]
}
```

This will deploy a server with a `ws://` prortocol. 

### Deploying

A server can be run locally as shown in `main.go` by initializing an `aura.Server{}` object,

```go
type Server struct {
    Domain          string
    Port            int
    Realms          []string
    IsTLS           bool
    RouterEvent     RouterEvent
    WSServer        *router.WebsocketServer
    RawSocket       *router.RawSocketServer
    Logger          *log.Logger
}
```

and calling `New()` from it's method.

eg.

```go
s := aura.Server{
    Domain: config.domain,
    Port: config.port,
    Realms: config.realms,
    Logger: logger,
}

s.New(aura.Option{Concurrent: true, Args: nil, Apply: aura.SetRouter})
```

## New(...Option)

Takes in an argument which is a struct,

```go
type Option struct {
	Concurrent bool
	Args       []interface{}
	Apply      func(s *Server, args []interface{}) error
}
```

Not happy with this but it allows for concurrence when needing to apply an option, basically with the router I need to set up a `chan` for it and keep it looping until it's time for it to shutdown. Look at the `RouterEvent` to see how. 

### Option
Basically it's a container for the self referential configuration function  so we can have an option be applied concurrently, 

eg
```go
func SetRouter(s *Server, args []interface{}) error // this needs to run concurrently.
```

## JavaScript

### Dependency Setup

> Example for JS clients can be found in the `example-clients` directory.
 
- **browser** - based code include a `<script src="https://unpkg.com/autobahn-browser@^18/autobahn.js"></script>` somewhere before your `</body>` closing tag.

- **NodeJS** - code you will need to add [AutobahJS](https://github.com/crossbario/autobahn-js) in your `package.json` or just `npm i -D autobahn`. 

### Example Code

```javascript

try {
   // for Node.js
   var autobahn = require('autobahn');
} catch (e) {
   // for browsers (where AutobahnJS is available globally)
}

var connection = new autobahn.Connection({url: 'ws://127.0.0.1:9000/', realm: 'realm1'});

connection.onopen = function (session) {

   // 1) subscribe to a topic
   function onevent(args) {
      console.log("Event:", args[0]);
   }
   session.subscribe('com.myapp.hello', onevent);

   // 2) publish an event
   session.publish('com.myapp.hello', ['Hello, world!']);

   // 3) register a procedure for remoting
   function add2(args) {
      return args[0] + args[1];
   }
   session.register('com.myapp.add2', add2);

   // 4) call a remote procedure
   session.call('com.myapp.add2', [2, 3]).then(
      function (res) {
         console.log("Result:", res);
      }
   );
};

connection.open();

```

## Python

- Todo

```python
from autobahn.twisted.wamp import ApplicationSession
# or: from autobahn.asyncio.wamp import ApplicationSession

class MyComponent(ApplicationSession):

    @inlineCallbacks
    def onJoin(self, details):

        # 1. subscribe to a topic so we receive events
        def onevent(msg):
            print("Got event: {}".format(msg))

        yield self.subscribe(onevent, 'com.myapp.hello')

        # 2. publish an event to a topic
        self.publish('com.myapp.hello', 'Hello, world!')

        # 3. register a procedure for remote calling
        def add2(x, y):
            return x + y

        self.register(add2, 'com.myapp.add2')

        # 4. call a remote procedure
        res = yield self.call('com.myapp.add2', 2, 3)
        print("Got result: {}".format(res))
```

## Java

From (Autobahn-Java)[https://github.com/crossbario/autobahn-java].

### Download

Grab via Maven:

```xml
<dependency>
    <groupId>io.crossbar.autobahn</groupId>
    <artifactId>autobahn-android</artifactId>
    <version>18.5.1</version>
</dependency>
```

Gradle:
```groovy
dependencies {
    implementation 'io.crossbar.autobahn:autobahn-android:18.5.1'
}
```
For non-android systems use artifactID `autobahn-java` or just
Download [the latest JAR](https://search.maven.org/remote_content?g=io.crossbar.autobahn&a=autobahn-java&v=LATEST)


### Getting Started

The demo clients are easy to run, you only need `Go`. Look at `Deploying the Router` section for setting up your client for testing.

To test using the `Autobahn-Java` examples you will need to `make` the demo that is supplied in it's repo package.

    $ make java # Starts the java (Netty) based demo client that performs WAMP actions

### Show me some code

The code in demo-gallery contains some examples on how to use the autobahn library, it also contains convenience methods to use. Below is a basic set of code examples showing all 4 WAMP actions.

#### Subscribe to a topic

```java
public void demonstrateSubscribe(Session session, SessionDetails details) {
    // Subscribe to topic to receive its events.
    CompletableFuture<Subscription> subFuture = session.subscribe("com.myapp.hello",
            this::onEvent);
    subFuture.whenComplete((subscription, throwable) -> {
        if (throwable == null) {
            // We have successfully subscribed.
            System.out.println("Subscribed to topic " + subscription.topic);
        } else {
            // Something went bad.
            throwable.printStackTrace();
        }
    });
}

private void onEvent(List<Object> args, Map<String, Object> kwargs, EventDetails details) {
    System.out.println(String.format("Got event: %s", args.get(0)));
}
```
Since we are only accessing `args` in onEvent(), we could simplify it like:
```java
private void onEvent(List<Object> args) {
    System.out.println(String.format("Got event: %s", args.get(0)));
}
```
#### Publish to a topic

```java
public void demonstratePublish(Session session, SessionDetails details) {
    // Publish to a topic that takes a single arguments
    List<Object> args = Arrays.asList("Hello World!", 900, "UNIQUE");
    CompletableFuture<Publication> pubFuture = session.publish("com.myapp.hello", args);
    pubFuture.thenAccept(publication -> System.out.println("Published successfully"));
    // Shows we can separate out exception handling
    pubFuture.exceptionally(throwable -> {
        throwable.printStackTrace();
        return null;
    });
}
```
A simpler call would look like:
```java
public void demonstratePublish(Session session, SessionDetails details) {
    CompletableFuture<Publication> pubFuture = session.publish("com.myapp.hello", "Hi!");
    ...
}
```

- Todo

## C++

- Todo

(Autobahn C++)[https://github.com/crossbario/autobahn-cpp]

## Go

- Todo

# We Are Currently Working To An Alpha Build

The aim of this utility is to manage the messages and payloads that are communicated back and forth from an eclectic range of components and modules. The goal is to reduce complexity for the developers of these components and the need to learn new code stacks to contribute. 

# Security Features

Security concerns can be addressed to [Rex](iamrexdev@gmail.com) or anyone on the project but Rex is the most paranoid.  

- **ACL's** - This router can intercept and monitor every communication sent to and from other components allowing  auth(z) to be configured on a range of levels. 

- **RBAC** - Each realm has it's own authenticator and can be leveraged to create various user roles and authorization. 

# Notes 

Each satellite component or module will need to adhere to the WAMP v2 communication protocol and examples can be found in the client-examples directory. 
